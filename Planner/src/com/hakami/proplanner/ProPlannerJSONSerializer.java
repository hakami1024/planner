package com.hakami.proplanner;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONTokener;

import android.content.Context;
import android.os.Environment;

public class ProPlannerJSONSerializer
{
	//private static final String TAG = "ProPlannerJSONSerializer";
	private Context mContext;
	private String mFilename;
	
	public ProPlannerJSONSerializer( Context c, String f )
	{
		mContext = c;
		mFilename = f;
	}
	
	public void saveWorks( ArrayList<Work> works ) throws JSONException, IOException
	{
		JSONArray array = new JSONArray();
		for( Work w: works )
			array.put( w.toJSON() );   
				
		if( Environment.getExternalStorageState().equals( Environment.MEDIA_MOUNTED ) )
			saveWorksExternally( array );
		else	
			saveWorksInternally( array );
	}
	
	public ArrayList<Work> loadWorks() throws IOException, JSONException
	{
		ArrayList<Work> works = new ArrayList<Work>();
		StringBuilder jsonString = Environment.getExternalStorageState().equals( Environment.MEDIA_MOUNTED ) ?
				loadWorksExternal() : loadWorksInternal();
		
		JSONArray array = (JSONArray) new JSONTokener( jsonString.toString() ).nextValue();
		for( int i=0; i< array.length(); i++ )
		{
			works.add( new Work(array.getJSONObject( i ) ) );
			
		}
		
		return works;
	}
	
	private void saveWorksInternally( JSONArray array )
			throws JSONException, IOException
	{	
		Writer writer = null;
		try{
			OutputStream out = null;
			out = mContext.openFileOutput( mFilename, Context.MODE_PRIVATE );
			writer = new OutputStreamWriter(out);
			writer.write( array.toString() );
		} finally{
			
			if( writer != null )
				writer.close();   
		}
	}
	
	private void saveWorksExternally( JSONArray array )
		throws JSONException, IOException
	{
		
		FileOutputStream out = null;
		try{
			File dir = new File( Environment.getExternalStorageDirectory().getAbsolutePath() 
					+ "/" + mContext.getPackageName() );
			dir.mkdirs();			
			File file = new File( dir, mFilename);

			out = new FileOutputStream( file );			
			out.write( array.toString().getBytes() );
		} finally{
			
			if( out != null )
				out.close();
		}
	}

	private StringBuilder loadWorksInternal() throws IOException, JSONException
	{
		BufferedReader reader = null;
		StringBuilder jsonString = new StringBuilder();
		
		try{
			InputStream in = mContext.openFileInput( mFilename );
			reader = new BufferedReader( new InputStreamReader(in) );
			
			String line = null;
			
			while( (line = reader.readLine()) != null )
				jsonString.append( line );
			
		}catch( FileNotFoundException e ){
		}finally{
			if( reader != null )
				reader.close();
		}
		
		return jsonString;
	}
	
	private StringBuilder loadWorksExternal() throws IOException, JSONException
	{
		StringBuilder jsonString = new StringBuilder();
		FileInputStream reader = null;
				
		try{
			reader = new FileInputStream( Environment.getExternalStorageDirectory().getAbsolutePath() 
											+ "/" + mContext.getPackageName() + "/" +mFilename );			
			
			byte[] buffer = new byte[1024];
			while( reader.read( buffer ) != -1 )
				jsonString.append( new String(buffer) );
			
			
		}catch( FileNotFoundException e ){
		}finally{
			if( reader != null )
				reader.close();
		}
		
		return jsonString;
	}
}
