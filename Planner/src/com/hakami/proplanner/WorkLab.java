package com.hakami.proplanner;

import java.util.ArrayList;
import java.util.UUID;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

public class WorkLab
{
	private static final String TAG = "WorkLab";
	private static String FILENAME = "works.json";
	
	private ArrayList<Work> mWorks;
	private ProPlannerJSONSerializer mSerializer;
	
	private static WorkLab sWorkLab;
	private Context mAppContext;
	
	private WorkLab( Context appContext )
	{
		mAppContext = appContext;
		
		
		mSerializer = new ProPlannerJSONSerializer( mAppContext, FILENAME );
		
		try{
			mWorks = mSerializer.loadWorks();
		}catch( Exception e ){
			Toast.makeText( mAppContext, "ERROR WAS THROWN WHILE LOAD FILES: "+e.toString(), Toast.LENGTH_LONG ).show();
			mWorks = new ArrayList<Work>();
		}
	}
	
	public static WorkLab get( Context c )
	{
		if( sWorkLab == null )
		{
			sWorkLab = new WorkLab( c.getApplicationContext() );
		}
		return sWorkLab;
	}
	
	public ArrayList<Work> getWorks()
	{
		return mWorks;
	}
	
	public Work getWork( UUID id )
	{
		for( Work w: mWorks )
		{
			if( w.getId().equals( id ) )
			{
				return w;
			}
		}
		return null;
	}
	
	public void addWork( Work w )
	{
		mWorks.add( w );
	}
	
	public boolean saveWorks()
	{
		try{
			mSerializer.saveWorks( mWorks );
			return true;
		}catch(Exception e){
			Log.d(TAG, "Error: can't save files in saveWorks()"+e.toString() );
			Toast.makeText( mAppContext, "ERROR WAS THROWN: "+e.toString(), Toast.LENGTH_LONG ).show();;
			return false;
		}
	}

	public void deleteWork( Work w)
	{
		mWorks.remove( w );
	}
}