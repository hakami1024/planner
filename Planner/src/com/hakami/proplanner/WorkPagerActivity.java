package com.hakami.proplanner;

import java.util.ArrayList;
import java.util.UUID;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

public class WorkPagerActivity extends FragmentActivity
{
	private ViewPager mViewPager;
	private ArrayList<Work> mWorks;
	
	@Override 
	public void onCreate( Bundle savedInstanceState )
	{
		super.onCreate( savedInstanceState );
		mViewPager = new ViewPager(this);
		mViewPager.setId( R.id.viewPager );
		setContentView( mViewPager );

		mWorks = WorkLab.get( this ).getWorks();
		
		FragmentManager fm = getSupportFragmentManager();
		mViewPager.setAdapter( new FragmentStatePagerAdapter( fm )
		{

			@Override
			public Fragment getItem( int pos )
			{
				Work work = mWorks.get( pos );
				return WorkFragment.newInstance( work.getId() );
			}

			@Override
			public int getCount()
			{
				return mWorks.size();
			}
			
		});
		
		mViewPager.setOnPageChangeListener( new ViewPager.OnPageChangeListener()
		{

			@Override
			public void onPageScrollStateChanged( int arg0 )
			{ }

			@Override
			public void onPageScrolled( int arg0, float arg1, int arg2 )
			{ }

			@Override
			public void onPageSelected( int pos )
			{
				Work work = mWorks.get(pos);
				if( work.getTitle() != null )
					setTitle( work.getTitle() );
			}
			
		});
		
		UUID workId = (UUID)getIntent().getSerializableExtra( WorkFragment.EXTRA_WORK_ID );
		for( int i=0; i<mWorks.size(); i++ )
			if( mWorks.get( i ).getId().equals( workId ) )
			{
				mViewPager.setCurrentItem( i );
				break;
			}
	}

}
