package com.hakami.proplanner;

import android.support.v4.app.Fragment;

public class WorkListActivity extends SingleFragmentActivity
{

	@Override
	protected Fragment createFragment()
	{
		return new WorkListFragment();
	}

}
