package com.hakami.proplanner;

import java.util.Date;
import java.util.UUID;

import org.json.JSONException;
import org.json.JSONObject;

public class Work
{
	private static final String JSON_ID = "id";
	private static final String JSON_TITLE = "title";
	private static final String JSON_SOLVED = "solved";
	private static final String JSON_DATE = "date";
	
	//private static final String TAG = "Work";
	private UUID mId;
	private String mTitle;
	private Date mDate;
	private boolean mDone;
	
	public Work()
	{
		mId = UUID.randomUUID();
		mDate = new Date();
	}

	public Work( JSONObject json ) throws JSONException
	{
		mId = UUID.fromString( json.getString( JSON_ID ) );
		mTitle = json.getString( JSON_TITLE );
		mDate = new Date( json.getLong( JSON_DATE ) );
		mDone = json.getBoolean( JSON_SOLVED );
	}
	/*
	@Override
	public String toString()
	{
		return mTitle;
	}
	*/
	public Date getDate()
	{
		return mDate;
	}

	public void setDate( Date date )
	{
		mDate = date;
	}

	public boolean isDone()
	{
		return mDone;
	}

	public void setDone( boolean done )
	{
		mDone = done;
	}

	public String getTitle()
	{
		return mTitle;
	}

	public void setTitle( String title )
	{
		mTitle = title;
	}

	public UUID getId()
	{
		return mId;
	}
	
	public JSONObject toJSON() throws JSONException
	{
		JSONObject json = new JSONObject();
		json.put( JSON_ID, mId.toString() );
		json.put( JSON_TITLE, mTitle );
		json.put( JSON_DATE, mDate.getTime() );
		json.put( JSON_SOLVED, mDone );
		return json;
	}
}
