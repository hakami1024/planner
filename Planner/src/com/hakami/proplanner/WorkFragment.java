package com.hakami.proplanner;

import java.util.Date;
import java.util.UUID;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;

public class WorkFragment extends Fragment
{
	public static final String EXTRA_WORK_ID = "com.hakami.proplanner.work_id";
	//private static final String TAG = "WorkFragment";
	private static final String DIALOG_DATE = "com.hakami.proplanner.date";
	private static final int REQUEST_DATE = 0;
	private Work mWork;
	private EditText mTitleField;
	private Button mDateButton;
	private CheckBox mDoneCheckBox;
	
	@Override
	public void onCreate( Bundle savedInstanceState )
	{
		super.onCreate( savedInstanceState );
		
		UUID workId = (UUID)getArguments().getSerializable( EXTRA_WORK_ID );
		mWork = WorkLab.get( getActivity() ).getWork( workId );
		
		setHasOptionsMenu(true);
	}
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@Override
	public View onCreateView( LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState )
	{
		View v = inflater.inflate( R.layout.fragment_work, parent, false );
		
		if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB )
			if( NavUtils.getParentActivityName( getActivity() ) != null )
			{
				getActivity().getActionBar().setDisplayHomeAsUpEnabled( true );
			}
		
		mTitleField = (EditText)v.findViewById( R.id.work_title );
		mTitleField.setText( mWork.getTitle() );
		mTitleField.addTextChangedListener( new TextWatcher()
		{

			@Override
			public void afterTextChanged( Editable arg0 ){}

			@Override
			public void beforeTextChanged( CharSequence s, int start,
					int count, int after ){}

			@Override
			public void onTextChanged( CharSequence s, int start, int before,
					int count )
			{
				mWork.setTitle( s.toString() );
			}
			
		});
		mDateButton = (Button)v.findViewById( R.id.work_date );
		//
		updateDate();
		mDateButton.setOnClickListener( new View.OnClickListener()
		{

			@Override
			public void onClick( View v )
			{
					FragmentManager fm = getActivity().getSupportFragmentManager();
					DatePickerFragment dialog = DatePickerFragment.newInstance( mWork.getDate() );
					dialog.setTargetFragment( WorkFragment.this, REQUEST_DATE );
					dialog.show( fm, DIALOG_DATE );
			}
			 
		});		
		mDoneCheckBox = (CheckBox)v.findViewById( R.id.work_done );
		mDoneCheckBox.setChecked( mWork.isDone() );
		mDoneCheckBox.setOnCheckedChangeListener( new OnCheckedChangeListener()
		{
			@Override
			public void onCheckedChanged( CompoundButton buttonView,
					boolean isChecked )
			{
				mWork.setDone( isChecked );
			}
			
		});
		return v;
	}

	@Override
	public void onPause()
	{
		super.onPause();
		WorkLab.get( getActivity() ).saveWorks();
	}
	
	public static WorkFragment newInstance( UUID workId )
	{
		Bundle args = new Bundle();
		args.putSerializable( EXTRA_WORK_ID, workId );
		WorkFragment fragment = new WorkFragment();
		fragment.setArguments( args );
		return fragment;
	}

	public void updateDate()
	{
		mDateButton.setText( DateFormat.format( "d MMMM yyyy, EEEE, kk:mm ", mWork.getDate()) );
	}
	
	@Override
	public void onActivityResult( int requestCode, int resultCode, Intent data )
	{
		if( resultCode != Activity.RESULT_OK )
			return;
		
		if( requestCode == REQUEST_DATE )
		{
			Date date = (Date)data.getSerializableExtra( DIALOG_DATE );
			mWork.setDate( date );
			updateDate();
		}
	}
	
	@Override
	public boolean onOptionsItemSelected( MenuItem item )
	{
		switch( item.getItemId() )
		{
			case android.R.id.home:
				if( NavUtils.getParentActivityName( getActivity() ) != null )
					NavUtils.navigateUpFromSameTask( getActivity() );
				return true;
			case R.id.menu_item_delete_work:
				WorkLab.get( getActivity() ).deleteWork( mWork );
				getActivity().setResult( Activity.RESULT_OK );
				getActivity().finish();
				return true;
			default:
				return super.onOptionsItemSelected( item );
		}
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater )
	{
		super.onCreateOptionsMenu( menu, inflater );
		inflater.inflate( R.menu.fragment_work, menu );

	}
}
