package com.hakami.proplanner;

import java.util.ArrayList;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.text.format.DateFormat;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

public class WorkListFragment extends ListFragment
{
	private static final int REQUEST_WORK = 1;
	
	private ArrayList<Work> mWorks;
	
	@Override
	public void onCreate( Bundle savedInstanceState )
	{
		super.onCreate( savedInstanceState );
		setHasOptionsMenu(true);
		
		getActivity().setTitle( R.string.works_title );
		mWorks = WorkLab.get( getActivity() ).getWorks();
		
		WorkAdapter adapter = new WorkAdapter( mWorks );
		setListAdapter( adapter );
		
		//getListView().setEmptyView( emptyView );
		
	}
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@Override
	public View onCreateView( LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState  )
	{
		View v = inflater.inflate( R.layout.fragment_works_list, parent, false );
		ListView listView = (ListView)v.findViewById( android.R.id.list );
		if( Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB )
			registerForContextMenu( listView );
		else 
		{
			listView.setChoiceMode( ListView.CHOICE_MODE_MULTIPLE_MODAL );
			listView.setMultiChoiceModeListener( new MultiChoiceModeListener()
			{

				@Override
				public boolean onActionItemClicked( ActionMode mode,
						MenuItem item )
				{
					switch( item.getItemId() )
					{
						case R.id.menu_item_delete_work:
							WorkAdapter adapter = (WorkAdapter)getListAdapter();
							WorkLab workLab = WorkLab.get( getActivity() );
							for( int i= adapter.getCount()-1; i>=0; i-- )
								if( getListView().isItemChecked( i ) )
									workLab.deleteWork( adapter.getItem( i ) );
							mode.finish();
							adapter.notifyDataSetChanged();
							return true;
						default:
							return false;
					}
				}

				@Override
				public boolean onCreateActionMode( ActionMode mode, Menu menu )
				{
					MenuInflater inflater = mode.getMenuInflater();
					inflater.inflate( R.menu.work_list_item_context, menu );
					return true;
				}

				@Override
				public void onDestroyActionMode( ActionMode mode ){}

				@Override
				public boolean onPrepareActionMode( ActionMode mode, Menu menu )
				{
					return false;
				}

				@Override
				public void onItemCheckedStateChanged( ActionMode mode,
						int position, long id, boolean checked ){}
				
			});
		}
		
		return v;
	}
	
	@Override
	public void onResume()
	{
		super.onResume();
		((WorkAdapter)getListAdapter()).notifyDataSetChanged();
	}
	
	@Override
	public void onListItemClick( ListView l, View v, int position, long id )
	{
		Work w = ((WorkAdapter)getListAdapter()).getItem( position );
		Intent i = new Intent( getActivity(), WorkPagerActivity.class );
		i.putExtra( WorkFragment.EXTRA_WORK_ID, w.getId() );
		//startActivity( i );
		startActivityForResult( i, REQUEST_WORK );
	}
	
	@Override
	public void onActivityResult( int requestCode, int resultCode, Intent data )
	{
		if( requestCode == REQUEST_WORK )
		{
			//Handle result
		}
	}
	
	private class WorkAdapter extends ArrayAdapter<Work>
	{
		public WorkAdapter( ArrayList<Work> works )
		{
			super( getActivity(), 0, works );
		}
		
		@Override
		public View getView( int position, View convertView, ViewGroup parent )
		{
			if( convertView == null )
			{
				convertView = getActivity().getLayoutInflater().inflate( R.layout.list_item_work, null );
			}
			
			Work w = getItem( position );
			
			TextView titleTextView = (TextView)convertView.findViewById( R.id.work_list_item_titleTextView );
			titleTextView.setText( w.getTitle() );
			
			TextView dateTextView = (TextView)convertView.findViewById( R.id.work_list_item_dateTextView );
			dateTextView.setText( DateFormat.format( "d MMMM yyyy, EEEE, kk:mm ", w.getDate() ) );
			
			CheckBox solvedCheckBox = (CheckBox)convertView.findViewById( R.id.work_list_item_solvedCheckBox );
			solvedCheckBox.setChecked( w.isDone() );
			
			return convertView;			
		}
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater )
	{
		super.onCreateOptionsMenu( menu, inflater );
		inflater.inflate( R.menu.fragment_work_list, menu );
	}

	@Override
	public boolean onOptionsItemSelected( MenuItem item )
	{
		switch( item.getItemId() )
		{
			case R.id.menu_item_new_work:
				Work work = new Work();
				WorkLab.get( getActivity() ).addWork( work );
				Intent i = new Intent( getActivity(), WorkPagerActivity.class );
				i.putExtra( WorkFragment.EXTRA_WORK_ID, work.getId() );
				startActivityForResult(i, 0);
				return true;
			default:
				return super.onOptionsItemSelected( item );
		}
	}

	@Override
	public void onCreateContextMenu( ContextMenu menu, View v, ContextMenuInfo menuInfo )
	{
		getActivity().getMenuInflater().inflate( R.menu.work_list_item_context, menu );
	}

	@Override
	public boolean onContextItemSelected( MenuItem item )
	{
		AdapterContextMenuInfo info = (AdapterContextMenuInfo)item.getMenuInfo();
		int position = info.position;
		WorkAdapter adapter = (WorkAdapter)getListAdapter();
		Work work = adapter.getItem( position );
		
		switch( item.getItemId() )
		{
			case R.id.menu_item_delete_work:
				WorkLab.get( getActivity() ).deleteWork( work );
				adapter.notifyDataSetChanged();
				return true;
		}
		return super.onContextItemSelected( item );
	}
}
