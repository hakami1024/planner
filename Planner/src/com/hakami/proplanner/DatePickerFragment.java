package com.hakami.proplanner;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.TimePicker;
import android.widget.TimePicker.OnTimeChangedListener;

public class DatePickerFragment extends DialogFragment
{
	
	public static final String EXTRA_DATE = "com.hakami.proplanner.date";
	
	private Date mDate;

	public static DatePickerFragment newInstance( Date date )
	{
		Bundle args = new Bundle();
		args.putSerializable( EXTRA_DATE, date );
		DatePickerFragment fragment = new DatePickerFragment();
		fragment.setArguments( args );
		
		return fragment;
	}
	
	private void sendResult( int resultCode )
	{
		if( getTargetFragment() == null )
			return;
		
		Intent i = new Intent();
		i.putExtra( EXTRA_DATE, mDate );
		
		getTargetFragment().onActivityResult( getTargetRequestCode(), resultCode, i );
		
	}

	@Override
	public Dialog onCreateDialog( Bundle savedInstanceState )
	{
		mDate = (Date)getArguments().getSerializable( EXTRA_DATE );
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime( mDate );
		int year = calendar.get(  Calendar.YEAR );
		int month = calendar.get( Calendar.MONTH );
		int day = calendar.get( Calendar.DAY_OF_MONTH );
		int hour = calendar.get( Calendar.HOUR_OF_DAY );
		int minute = calendar.get( Calendar.MINUTE );
		
		View v = getActivity().getLayoutInflater().inflate( R.layout.time_date_dialog, null );
		
		DatePicker datePicker = (DatePicker)v.findViewById( R.id.datePicker1 );
		datePicker.init( year, month, day, new OnDateChangedListener()
		{

			@Override
			public void onDateChanged( DatePicker view, int year,
					int month, int day )
			{
				mDate = new GregorianCalendar( year, month, day ).getTime();
				getArguments().putSerializable( EXTRA_DATE, mDate );
			}
			
		});
		
		TimePicker timePicker = (TimePicker)v.findViewById( R.id.timePicker1 );
		timePicker.setIs24HourView( true );
		timePicker.setCurrentHour( hour );
		timePicker.setCurrentMinute( minute );
		timePicker.setOnTimeChangedListener( new OnTimeChangedListener()
		{

			@Override
			public void onTimeChanged( TimePicker view, int hourOfDay,
					int curMinute )
			{
				Calendar calendar = Calendar.getInstance();
				calendar.setTime( mDate );
				calendar.set( Calendar.HOUR_OF_DAY, hourOfDay );
				calendar.set( Calendar.MINUTE, curMinute );
				mDate = calendar.getTime();
			}
			
		});
		
		return new AlertDialog.Builder( getActivity() )
		.setView( v )
		//.setTitle(R.string.date_picker_title)
		.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener()
		{
			
			@Override
			public void onClick( DialogInterface dialog, int which )
			{
				sendResult( Activity.RESULT_OK );
			}
		})
		.create();
	}

}
